# Write a function solution(area) that takes
# the total area of solar panels you have (between 1 and 1000000 inclusive)
# and returns a list of the areas of the largest squares you could make out of those panels,
# starting with the largest squares first. Example, solution(12) would return [9, 1, 1, 1].

import math

def solution(area):
    areas = []
    while area != 0:
        square = largest_square(area)
        areas.append(square)
        area -= square
    return areas


def largest_square(number):
    for i in reversed(range(number+1)):
        if math.sqrt(i).is_integer():
            return i


if __name__ == "__main__":

    assert solution(12) == [9, 1, 1, 1]
    assert solution(15324) == [15129,169,25,1]
