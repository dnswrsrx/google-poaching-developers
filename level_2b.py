# Given a list of number, what is the largest number divisible by 3 that can be made

from itertools import permutations

def solution(l):
    l = ''.join([str(char) for char in l])
    for length in reversed(range(1, len(l)+1)):
        numbers = sorted(
            map(lambda x: int(''.join(x)), permutations(l, length)),
            reverse=True
        )
        for number in numbers:
            if number % 3 == 0:
                return number
    return 0


if __name__ == "__main__":
    assert solution([3, 1, 4, 1]) == 4311
    assert solution([3, 1, 4, 1, 5, 9]) == 94311
    assert solution([4]) == 0

